import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import DiffieHellman from './diffie-hellman';
import BigInt from 'big-integer'
import CryptoJS from 'crypto-js'
import crypto from 'crypto'

const app = express();
const server = http.createServer(app);
const io = socketIO(server);

const privateKey = DiffieHellman.generator()
let publicKey,g,p = null

app.use(express.static(__dirname + '/dist'));
app.get('/', (req, res, next) =>
{
    res.sendFile(__dirname + '/dist/index.html');
});

server.listen(4200, () => `Listening on http://localhost:4200/`);

function getCryptoNum()
{

    p = DiffieHellman.generator()
    g = DiffieHellman.generator()
    publicKey = DiffieHellman.computePartialKey(privateKey, p, g)
}

io.on('connection', (socket) =>
{
    socket.on('request_prime', function(data)
    {
        getCryptoNum()
        socket.emit('receivePrime',
        {
            p,
            g,
            publicKey
        })
    })

    socket.on('message', (data) =>
    {
        const completeKey = DiffieHellman.completeKey(privateKey, p, BigInt(data.key))
        const message = CryptoJS.AES.decrypt(data.message, completeKey)
            .toString(CryptoJS.enc.Utf8)
        const response = CryptoJS.AES.encrypt(`- ${message}\n - Are we chating?\n`, completeKey)
            .toString()
        socket.emit('response', response)
    })
});