import BigInt from 'big-integer'
import crypto from 'crypto'

export default class DiffieHellman{
    constructor(){}
    
    static generator(){
        const prime = crypto.createDiffieHellman(84)
        const l = prime.getPrime('hex').toString()
        return BigInt(l,16)
    }

    static publicPrime(){
        return DiffieHellman.generator()
    }
    /**
     * 
     * @param {BigInt} pk 
     * @param {BigInt} p 
     * @param {BigInt} g 
     */
    static computePartialKey(pk, p, g){
        return g.modPow(pk,p).toString()
    }
    /**
     * 
     * @param {BigInt} pk 
     * @param {BigInt} p 
     * @param {BigInt} partialKey 
     */
    static completeKey(pk, p, partialKey){
        const K  = partialKey
        return K.modPow(pk,p).toString()
    }
} 