'use strict';

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _socket = require('socket.io');

var _socket2 = _interopRequireDefault(_socket);

var _diffieHellman = require('./diffie-hellman');

var _diffieHellman2 = _interopRequireDefault(_diffieHellman);

var _bigInteger = require('big-integer');

var _bigInteger2 = _interopRequireDefault(_bigInteger);

var _cryptoJs = require('crypto-js');

var _cryptoJs2 = _interopRequireDefault(_cryptoJs);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express2.default)();
var server = _http2.default.createServer(app);
var io = (0, _socket2.default)(server);

var privateKey = _diffieHellman2.default.generator();
var publicKey = void 0,
    g = void 0,
    p = null;

app.use(_express2.default.static(__dirname + '/dist'));
app.get('/', function (req, res, next) {
    res.sendFile(__dirname + '/dist/index.html');
});

server.listen(4200, function () {
    return 'Listening on http://localhost:4200/';
});

function getCryptoNum() {

    p = _diffieHellman2.default.generator();
    g = _diffieHellman2.default.generator();
    publicKey = _diffieHellman2.default.computePartialKey(privateKey, p, g);
}

io.on('connection', function (socket) {
    socket.on('request_prime', function (data) {
        getCryptoNum();
        socket.emit('receivePrime', {
            p: p,
            g: g,
            publicKey: publicKey
        });
    });

    socket.on('message', function (data) {
        var completeKey = _diffieHellman2.default.completeKey(privateKey, p, (0, _bigInteger2.default)(data.key));
        var message = _cryptoJs2.default.AES.decrypt(data.message, completeKey).toString(_cryptoJs2.default.enc.Utf8);
        var response = _cryptoJs2.default.AES.encrypt('- ' + message + '\n - Are we chating?\n', completeKey).toString();
        socket.emit('response', response);
    });
});
