'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _bigInteger = require('big-integer');

var _bigInteger2 = _interopRequireDefault(_bigInteger);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var DiffieHellman = function () {
    function DiffieHellman() {
        _classCallCheck(this, DiffieHellman);
    }

    _createClass(DiffieHellman, null, [{
        key: 'generator',
        value: function generator() {
            var prime = _crypto2.default.createDiffieHellman(84);
            var l = prime.getPrime('hex').toString();
            return (0, _bigInteger2.default)(l, 16);
        }
    }, {
        key: 'publicPrime',
        value: function publicPrime() {
            return DiffieHellman.generator();
        }
        /**
         * 
         * @param {BigInt} pk 
         * @param {BigInt} p 
         * @param {BigInt} g 
         */

    }, {
        key: 'computePartialKey',
        value: function computePartialKey(pk, p, g) {
            return g.modPow(pk, p).toString();
        }
        /**
         * 
         * @param {BigInt} pk 
         * @param {BigInt} p 
         * @param {BigInt} partialKey 
         */

    }, {
        key: 'completeKey',
        value: function completeKey(pk, p, partialKey) {
            var K = partialKey;
            return K.modPow(pk, p).toString();
        }
    }]);

    return DiffieHellman;
}();

exports.default = DiffieHellman;
